const $ = document.querySelector.bind(document);
const $$ = document.querySelectorAll.bind(document);

const trackers = [
  $('[name=username]'),
  $('[name=email]'),
  $('[name=password]')
]

$('form').addEventListener('submit', function(e) {
  e.preventDefault()
  trackers.forEach(el => el.dispathEvent(new Event('input')))
  
  // If there's element with error class, don't submit
  if($('.error')) return;
  this.submit();
})

trackers.forEach(el => {
  el.addEventListener('input', function() {
    const helper = el.parentElement.nextElementSibling;
    let error = null;
    switch(el.type) {
      case 'text':
        if(el.value.length < 3) {
          error = 'Must be at least 3 characters long'
          break
        }
        if(el.value.length > 20) {
          error = 'Must be less than 20 characters long'
          break
        }
        break;
      case 'email':
        if(!el.value.includes('@')) {
          error = 'No @ found in the email'
          break
        }
        if(!el.value.match(/^[a-z\d][a-z\d\.\_\+]{3,}[a-z\d]@(?:[a-z\d]{2,}\.)+[a-z]{2,}$/)) {
          error = 'Must be a valid email'
          break;
        }
        break;
      case 'password':
        if(el.value.length < 6) {
          error = 'Must be at least 6 characters long'
          break
        }
        if(el.value.length > 20) {
          error = 'Must be less than 20 characters long'
          break
        }
        if (!el.value.match(/[a-z]/)) {
          error = 'Must contain at least one lowercase letter'
          break;
        }
        if (!el.value.match(/[A-Z]/)) {
          error = 'Must contain at least one uppercase letter'
          break;
        }
        if (!el.value.match(/[0-9]/)) {
          error = 'Must contain at least one number'
          break;
        }
        if (!el.value.match(/[^a-zA-Z0-9]/)) {
          error = 'Must contain at least one special character'
          break;
        }
        break;
    }
    if (error) {
      el.classList.add('error')
      helper.innerText = error
      helper.classList.add('visible')
    } else {
      el.classList.remove('error')
      helper.classList.remove('visible')
    }
  })
})
